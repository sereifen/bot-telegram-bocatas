﻿using System;
using System.Collections.Generic;
using System.Timers;
using Telegram.Bot;
using Telegram.Bot.Types.ReplyMarkups;

namespace TelegramBotLaunch
{
    class Program
    {
        private static TelegramBotClient _bot;
        private static readonly Dictionary<string, Dictionary<string, Tuple<string, string>>> Pedidos = new Dictionary<string, Dictionary<string, Tuple<string, string>>>();
        private static readonly List<string> Menu = new List<string>()
        {
            "Truita (1.90€/3.20€)",
            "Truita Formatge(2.30€/3.90€)",
            "Buti(???/4.20€)",
            "Buti Formatge(???/4.90€)",
            "Hamburgesa(???/???)",
            "Hamburgesa Formatge(???/???)",
            "Llom(1.90€/3.20€)",
            "Llom Formatge(2.30€/3.90€)",
            "Iberic(3.95€/70€)",
            "Bacon(1.90€/3.10€)",
            "Bacon Formatge(2.30€/3.80€)",
            "Longaniza (1.90€/???)"
        };
        private static readonly Timer ATimer = new Timer(30 * 1000);
        private static readonly List<Tuple<long, int>> MsgToBeDeleted = new List<Tuple<long, int>>();

        private static void Main(String[] args)
        {
            if (args.Length != 1)
            {
                Console.WriteLine("Invalid parameters");
                return;
            }
            _bot = new TelegramBotClient(args[0]);
            _bot.OnMessage += Bot_OnMessageAsync;

            ATimer.Elapsed += OnTimedEvent;
            ATimer.AutoReset = true;
            ATimer.Enabled = true;

            _bot.StartReceiving();
            Console.ReadLine();
            _bot.StopReceiving();
        }

        private static async void Bot_OnMessageAsync(object sender, Telegram.Bot.Args.MessageEventArgs e)
        {
            if (e.Message.Chat.Type != Telegram.Bot.Types.Enums.ChatType.Supergroup) return;
            if (e.Message.Type == Telegram.Bot.Types.Enums.MessageType.Text)
            {
                try
                {
                    if (!Pedidos.ContainsKey(e.Message.Chat.Title)) Pedidos.Add(e.Message.Chat.Title, new Dictionary<string, Tuple<string, string>>());
                    if (e.Message.Text.ToLower() == "/entrepa")
                    {
                        await _bot.DeleteMessageAsync(e.Message.Chat.Id, e.Message.MessageId);
                        if (!Pedidos[e.Message.Chat.Title].ContainsKey(e.Message.From.Username)) Pedidos[e.Message.Chat.Title].Add(e.Message.From.Username, new Tuple<string, string>("", ""));
                        var rkm = new ReplyKeyboardMarkup
                        {
                            Keyboard = new[]
                            {
                                new[] {new KeyboardButton("Gran"), new KeyboardButton("Mitja")},
                                new[] {new KeyboardButton("Cancelar")}
                            }
                        };

                        var respuesta = await _bot.SendTextMessageAsync(e.Message.Chat.Id, "Com el vols?", Telegram.Bot.Types.Enums.ParseMode.Default, false, false, 0, rkm);
                        ATimer.Stop();
                        OnTimedEvent(null, null);
                        MsgToBeDeleted.Add(new Tuple<long, int>(respuesta.Chat.Id, respuesta.MessageId));
                        ATimer.Start();
                    }
                    else if (e.Message.Text.ToLower() == "/borrarentrepans" && e.Message.From.Username == "Sereifen")
                    {
                        await _bot.DeleteMessageAsync(e.Message.Chat.Id, e.Message.MessageId);
                        Pedidos[e.Message.Chat.Username] = new Dictionary<string, Tuple<string, string>>();
                    }
                    else if (e.Message.Text.ToLower() == "/borrarelmeuentrepa")
                    {
                        await _bot.DeleteMessageAsync(e.Message.Chat.Id, e.Message.MessageId);
                        Pedidos[e.Message.Chat.Title].Remove(e.Message.From.Username);
                    }
                    else if (e.Message.Text.ToLower() == "/llistaentrepans")
                    {
                        await _bot.DeleteMessageAsync(e.Message.Chat.Id, e.Message.MessageId);

                        String llista = "";
                        foreach (var pedido in Pedidos[e.Message.Chat.Title])
                        {
                            llista += "\n" + pedido.Key + " ha demanat " + pedido.Value.Item2 + " " + pedido.Value.Item1;
                        }
                        if (llista == "") llista = "No hi ha res guardat";
                        var respuesta = await _bot.SendTextMessageAsync(e.Message.Chat.Id, llista);
                        ATimer.Stop();
                        OnTimedEvent(null, null);
                        MsgToBeDeleted.Add(new Tuple<long, int>(respuesta.Chat.Id, respuesta.MessageId));
                        ATimer.Start();
                    }
                    else if (e.Message.Text.ToLower() == "/llistacomanda")
                    {
                        await _bot.DeleteMessageAsync(e.Message.Chat.Id, e.Message.MessageId);

                        String llista = "";
                        var agrup = new Dictionary<string, Dictionary<String, int>>();
                        foreach (var pedido in Pedidos[e.Message.Chat.Title])
                        {
                            if (!agrup.ContainsKey(pedido.Value.Item1))
                                agrup.Add(pedido.Value.Item1, new Dictionary<string, int>());
                            if (!agrup[pedido.Value.Item1].ContainsKey(pedido.Value.Item2))
                                agrup[pedido.Value.Item1].Add(pedido.Value.Item2, 1);
                            else
                                agrup[pedido.Value.Item1][pedido.Value.Item2] += 1;
                        }
                        var total = 0;
                        foreach (var conjunto in agrup)
                        {
                            var cantidad = 0;
                            foreach (var tipo in conjunto.Value) cantidad += tipo.Value;
                            total += cantidad;
                            llista += conjunto.Key +" (" + cantidad + ")\n";
                            foreach (var tipo in conjunto.Value)
                            {
                                llista += "    " + tipo.Key + "  :  " + tipo.Value + "\n";
                            }
                        }

                        if (total == 0) llista = "No hi ha res guardat";
                        else llista += "Total: " + total + ""; 
                        var respuesta = await _bot.SendTextMessageAsync(e.Message.Chat.Id, llista);
                        ATimer.Stop();
                        OnTimedEvent(null, null);
                        MsgToBeDeleted.Add(new Tuple<long, int>(respuesta.Chat.Id, respuesta.MessageId));
                        ATimer.Start();
                    }
                    else if (e.Message.Text.ToLower() == "/ajuda")
                    {
                        await _bot.DeleteMessageAsync(e.Message.Chat.Id, e.Message.MessageId);
                        var respuesta = await _bot.SendTextMessageAsync(e.Message.Chat.Id, "Les comandes vàlides són:\n" +
                            "    * /Entrepa: Per a seleccionar el teu entrepà\n" +
                            "    * /BorrarEntrepans: Esborra tots els entrepans, però això només ho pot fer el Calmet\n" +
                            "    * /BorrarElMeuEntrepa: Esborra el teu entrepà\n" +
                            "    * /LlistaEntrepans: Et dóna el llistat d'entrepans\n" +
                            "    * /LlistaComanda: Et dóna el llistat d'entrepans agrupats per a què et sigui fàcil trucar\n" +
                            "    * /Ajuda: Doncs això, et dono totes les comandes" +
                            "\nSi vols contribuir, pots pagar-me un entrepà");
                        ATimer.Stop();
                        OnTimedEvent(null, null);
                        MsgToBeDeleted.Add(new Tuple<long, int>(respuesta.Chat.Id, respuesta.MessageId));
                        ATimer.Start();
                    }
                    else if ((e.Message.Text == "Gran" || e.Message.Text == "Mitja") && Pedidos[e.Message.Chat.Title].ContainsKey(e.Message.From.Username))
                    {
                        await _bot.DeleteMessageAsync(e.Message.Chat.Id, e.Message.MessageId);
                        Pedidos[e.Message.Chat.Title][e.Message.From.Username] = new Tuple<string, string>(e.Message.Text, "");
                        var rkm = new ReplyKeyboardMarkup {Keyboard = GetMenu()};
                        var respuesta = await _bot.SendTextMessageAsync(e.Message.Chat.Id, "De que el vols?", Telegram.Bot.Types.Enums.ParseMode.Default, false, false, 0, rkm);
                        ATimer.Stop();
                        OnTimedEvent(null, null);
                        MsgToBeDeleted.Add(new Tuple<long, int>(respuesta.Chat.Id, respuesta.MessageId));
                        ATimer.Start();
                    }
                    else if (Menu.Contains(e.Message.Text) && Pedidos[e.Message.Chat.Title].ContainsKey(e.Message.From.Username))
                    {
                        await _bot.DeleteMessageAsync(e.Message.Chat.Id, e.Message.MessageId);
                        Pedidos[e.Message.Chat.Title][e.Message.From.Username] = new Tuple<string, string>(Pedidos[e.Message.Chat.Title][e.Message.From.Username].Item1, e.Message.Text);
                        var respuesta = await _bot.SendTextMessageAsync(e.Message.Chat.Id, "Gracies " +
                            e.Message.From.Username +
                            " guardo el teu entrepa " +
                            Pedidos[e.Message.Chat.Title][e.Message.From.Username].Item1 +
                            " de " +
                            Pedidos[e.Message.Chat.Title][e.Message.From.Username].Item2);
                        ATimer.Stop();
                        OnTimedEvent(null, null);
                        MsgToBeDeleted.Add(new Tuple<long, int>(respuesta.Chat.Id, respuesta.MessageId));
                        ATimer.Start();
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    Console.WriteLine(ex.StackTrace);
                    Console.WriteLine(ex.Source);
                }
            }
        }

        private static void OnTimedEvent(object sender, ElapsedEventArgs e)
        {
            while (MsgToBeDeleted.Count != 0)
            {
                try
                {
                    _bot.DeleteMessageAsync(MsgToBeDeleted[0].Item1, MsgToBeDeleted[0].Item2);
                    MsgToBeDeleted.RemoveAt(0);
                }
                catch
                {
                    // ignored
                }
            }
        }

        private static IEnumerable<IEnumerable<KeyboardButton>> GetMenu()
        {
            var menuButtonses = new List<KeyboardButton[]>();
            foreach (var men in Menu)
            {
                menuButtonses.Add(new[]
                    {
                        new KeyboardButton(Menu[0])
                    });
            }

            return menuButtonses;
        }
    }
}
